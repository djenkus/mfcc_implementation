# MFCC feature extraction for MSP430FR5994

MFFC pipeline functions are integrated in software example from TI called BOOSTXL-AUDIO_RecordPlayback_MSP430FR5994

### Files:
*  The main project files are located in application folder.
*  The MFFC pipeline is split into multiple tasks found in mfcc.c.
*  The pipeline functions are called in the main application loop located in application.c file.

### Resources

* [Audio Module]( http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/BOOSTXL-AUDIO/latest/index_FDS.html) - Audio module used for testing the pipeline
* [TI's Software Examples](software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP-EXP430FR5994/latest/exports/MSP-EXP430FR5994_Software_Examples_linux.tar.gz) - Includes software example for audio module

