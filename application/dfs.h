/*
 * dfs.h
 *
 *  Created on: 1 Jul 2019
 *      Author: DAINIUS
 */

#ifndef DFS_H_
#define DFS_H_


#include <driverlib.h>
#include <stdio.h>
#include <stdlib.h>




extern void set_MCLK_freq_1(void);
extern void set_MCLK_freq_2(void);
extern void set_MCLK_freq_3(void);
extern void set_MCLK_freq_4(void);
extern void set_MCLK_freq_5(void);
extern void set_MCLK_freq_6(void);
extern void set_MCLK_freq_8(void);
extern void set_MCLK_freq_16(void);





#endif /* DFS_H_ */
