/*
 * mfcc.c
 *
 *  Created on: 19 Jul 2019
 *      Author: Dainius Jenkus
 */


// Includes
//******************************************************************************
#include <stdlib.h>
#include <stdint.h>
#include <driverlib.h>
#include <math.h>

#include "audio_collect.h"
#include "global.h"
#include "dac8311.h"
#include "DSPLib.h"
#include "IQmathLib.h"
#include "mfcc.h"



#define OPTIMISED

/* Constants */
#define PI                  3.1415926536

#define pre_emphasis 0.97
#define pre_emphasis_Q15 _Q15(pre_emphasis)

#define sample_rate         8000
#define numCepstra 12
#define nfilt 26
#define fb_len (frame_length / 2) + 1


// Used in logMelFilterBank()
#define lmfbCoef_scale_1024 0.0009765625
#define maxNonZeroFbank 19

_iq8 lmfCoefTemp;


// Used in apply_DCT()
#define f0      0.0980580673
#define fn      0.138675049
#define fn_x_2     0.277350098
#define f0_fn_ratio 0.707106779533209
#define fn_x_1024 142.003250176

#define indexMaxVal 246


/*Variables in shared LEA RAM*/

// Single frame
DSPLIB_DATA(frameSample, 4)
_q15 frameSample[frame_length];


DSPLIB_DATA(mfccDSP, 4)
_iq31 mfccDSP[numCepstra+1][1];


DSPLIB_DATA(cosinesDSP, 4)
_iq31 cosinesDSP[numCepstra+1][nfilt];

DSPLIB_DATA(lmfbCoefDSP, 4)
_iq31 lmfbCoefDSP[nfilt][1];

/*  FFT result */
DSPLIB_DATA(fft_data,MSP_ALIGN_FFT_Q15(frame_length))
//DSPLIB_DATA(fft_data, MSP_ALIGN_FFT_IQ31(frame_length))
int16_t fft_data[frame_length];


DSPLIB_DATA(result,4)
_q15 result[frame_length];


/*Variables stored in FRAM*/

//#pragma PERSISTENT(pow_spectrum);
//float pow_spectrum[fb_len] = {0};

#pragma PERSISTENT(pow_spectrumIQ8);
_iq8 pow_spectrumIQ8[fb_len] = {0};

//#pragma PERSISTENT(indexes);
//int16_t indexes[nfilt][maxNonZeroFbank] = {0};

#pragma PERSISTENT(indexList);
uint16_t indexList[indexMaxVal] = {0};



#pragma PERSISTENT(fbank);
float fbank[nfilt][fb_len] = {0};

#pragma PERSISTENT(lmfbCoef);
float lmfbCoef[nfilt] = {0};

#pragma PERSISTENT(lmfbCoefQ31);
_iq31 lmfbCoefQ31[nfilt] = {0};

#pragma PERSISTENT(cosinesQ31);
_iq31 cosinesQ31[numCepstra+1][nfilt] = {0};

#pragma PERSISTENT(mfccs);
float mfccs[numCepstra+1] = {0};

#pragma PERSISTENT(cosines);
float cosines[numCepstra+1][nfilt] = {0};


#pragma PERSISTENT(fbankIQ8);
_iq8 fbankIQ8[nfilt][fb_len] = {0};


/* Benchmark cycle counts */
uint32_t cycleCounter =0;
int16_t i;
int16_t k;


/* DSPLib configuration parameter variables*/

msp_status status;

msp_sub_q15_params sub_params;
msp_scale_q15_params scaleShift;
msp_fill_q15_params fill_params;
msp_mpy_q15_params mpyParams;
msp_fft_q15_params  fftParams;
msp_abs_q15_params abs_params;

// Complex FFT Parameters
msp_cmplx_fft_q15_params cfftParams;
msp_cmplx_fill_q15_params cfillParams;
msp_interleave_q15_params interleaveParams;


/* Variables for emph_filter() function */

_q15 shifted1, shifted2;

/* Variables for real FFT function */
uint16_t shiftVal = 1;
uint16_t* shift = &shiftVal;


// apply_DCT function config for matrix multiplication
msp_matrix_mpy_iq31_params iq31_params;



/* Variables for power spectrum function */
int16_t add = 0;
int16_t addCntr = 0;
int32_t tmpR = 0;

/* Variables for logMelFilterBank function */

//_iq8 qTemp[26];

uint16_t indCounter = 0;
uint16_t filtNum = 0;


void initCosines(){

    // Cosine values used for DCT

    for (i=0; i<numCepstra+1; i++) {

        for (k=0; k<nfilt; k++){
            cosines[i][k] = cos(PI * i * (2.0*k +1)/(2.0 * nfilt));
            cosinesQ31[i][k] = _IQ31(cosines[i][k]);

        }
    }

}


void generate_filter_bank(){

    /*
    Function generates filter bank coefficients (triangular filters) , which are later applied
    to the power spectrum.
    Fbanks: 20-40, default = 26 (standard).
    */

    float high_freq_mel;
    float low_freq_mel = 0;


    high_freq_mel = 2595 * log10(1 + ((sample_rate * 0.5) / 700));  //  # Convert Hz to Mel

    float spacing = (high_freq_mel - low_freq_mel) / (float)(nfilt + 1);     //# To make equally spaced in Mel scale

    float mel_points[nfilt+2];
    float hz_points[nfilt+2];
    float bin[nfilt+2];
    float tmp;

    int i;


    for(i=0;i<nfilt+2;i++)
    {
       mel_points[i] = spacing*(float)i;
       tmp = mel_points[i] / 2595;
       hz_points[i] = 700.0 * (pow((double)10.0, tmp) -1.0);  //Convert Mel to Hz
       bin[i] = floor(((frame_length + 1) * hz_points[i]) / sample_rate);

    }


    int fi_left, fi_right, fi_center;
    int k;


    for(i=1;i<nfilt+1;i++)
    {
        fi_left = (int)bin[i-1];
        fi_center = (int)bin[i];
        fi_right = (int)bin[i+1];


        for(k = fi_left; k<fi_center; k++){
            fbank[i -1][k] = (k- bin[i -1]) / (bin[i] - bin[i-1]);

            fbankIQ8[i -1][k] = _IQ8(fbank[i -1][k]);
        }
        for(k = fi_center; k<fi_right; k++){
            fbank[i -1][k] = (bin[i + 1] - k) / (bin[i + 1] - bin[i]);

            fbankIQ8[i -1][k] = _IQ8(fbank[i -1][k]);
        }
    }


    /* Added to select only filter banks with only non-zero values*/

    int notZeros= 0;
    int notZ = 0;

       for ( i=0; i<nfilt; i++) {
           notZeros = 0;
           // Multiply the filterbank matrix
           for (k=0; k<fb_len; k++){
               if(fbank[i][k] != 0){
                   //indexes[i][notZeros] = k;
                   //notZeros++;
                   indexList[notZ] = k;
                   notZ++;
               }
           }
           indexList[notZ] = 0;
           notZ++;
       }

}


void init_msp_sub_q15(uint16_t length){

    /*Configuration for msp_sub_q15 function */
    sub_params.length = length;


}

void init_msp_scale_q15(uint16_t length, uint8_t shift, _q15 scale){

    /*Configuration for msp_scale_q15 */

    scaleShift.length = length;
    scaleShift.shift = shift;   // (only used for scaling )
    scaleShift.scale = scale;

}


void init_msp_mpy_q15(uint16_t length){
    /*Configuration for  msp_mpy_q15 function */
    mpyParams.length  = length;

}

void CONFIGURE_DMA_TRANSFER_FROM_FRAM_TO_RAM(){


    // Function configures DMA to transfer a frame from FRAM to RAM

    // For safety, protect RMW Cpu instructions
    DMA_disableTransferDuringReadModifyWrite();

    DMA_initParam param = {0};
    param.channelSelect = DMA_CHANNEL_3;
    param.transferModeSelect = DMA_TRANSFER_REPEATED_BLOCK;

    //param.transferModeSelect = DMA_TRANSFER_BLOCK;
    param.transferSize = frame_length;
    param.triggerSourceSelect = DMA_TRIGGERSOURCE_0;
    param.transferUnitSelect = DMA_SIZE_SRCWORD_DSTWORD;
    param.triggerTypeSelect = DMA_TRIGGER_RISINGEDGE;

    DMA_init(&param);

    DMA_setSrcAddress(DMA_CHANNEL_3, 0x10000, DMA_DIRECTION_INCREMENT);
    DMA_setDstAddress(DMA_CHANNEL_3, (uint32_t)&frameSample[0], DMA_DIRECTION_INCREMENT);


}


void emph_filter(void)
{
    /*
     Function scales a frame by pre_emphasis_Q15.
     Then a right shifted result is substracted from the original frame

     Function task selection:
     msp_matrix_scale_q15 - not supported by LEA and is performed on the CPU
     msp_matrix_sub_q15 - LEA and CPU
     Shifting - CPU

     Estimation: y(t) = x(t) - ax(t-1)
    */

//    // Enable DMA channel 1 interrupt
//    DMA_enableInterrupt(DMA_CHANNEL_3);
//    // Enable the DMA0 to start receiving triggers when ADC sample available
    DMA_enableTransfers(DMA_CHANNEL_3);
    //DMA_clearInterrupt(DMA_CHANNEL_3);
    DMA_startTransfer(DMA_CHANNEL_3);
//  //Disable DMA channel and interrupt
    DMA_disableTransfers(DMA_CHANNEL_3);
//  DMA_disableInterrupt(DMA_CHANNEL_3);


    init_msp_sub_q15(frame_length);
    init_msp_scale_q15(frame_length, 0, pre_emphasis_Q15);

    // Copy frame from FRAM to LEA RAM
    //memcpy(frameSample, dataRecorded1, sizeof(dataRecorded1));

    // Find ax(t)
    status = msp_scale_q15(&scaleShift, &frameSample[0], &result[0]);

    // y(t) = x(t) - ax(t-1)

    //mfccDSP[0][0] = _IQ31(1.0);

#if defined(MSP_DISABLE_LEA)

    shifted1 = result[0];

    // Shift result to align for subtraction step to get ax(t-1)
    for(i=1;i<frame_length;i++){

       shifted2 = result[i];
       result[i] = shifted1;
       shifted1 = shifted2;
    }

    result[0] = 0;

    status = msp_sub_q15(&sub_params, &frameSample[0], &result[0], &result[0]);

#else // CPU subtraction without shifting step

    status = msp_sub_q15(&sub_params, &frameSample[1], &result[0], &result[0]);
    msp_checkStatus(status);


#endif

}

void apply_hamming(){

    /*
     Function multiplies a frame by the hamming window
     Function task selection: msp_mpy_q15() can run on LEA and CPU
     */


    // Copy results from emph-filter without filling [0] element
    memcpy(&fft_data[1], result, sizeof(result));
    // set y(0) = x(0) since there were no x(t-1) in y(t) = x(t) - ax(t-1)
    fft_data[0] = frameSample[0];

    // Copy hamming window from FRAM to LEA RAM
    memcpy(frameSample, hammingWindow, sizeof(hammingWindow));

    init_msp_mpy_q15(frame_length);

    // Multiply by hamming window
    status = msp_mpy_q15(&mpyParams, fft_data, frameSample, fft_data);
    //msp_checkStatus(status);

}

void apply_FFT(){

    /* Parameters for 256-point FFT*/
    fftParams.length = frame_length;
    // if inputs are not in bit reversed order, the bit reversal parameter must be set
    fftParams.bitReverse = true;
    // Can be null for LEA, only required for the CPU
    fftParams.twiddleTable = msp_cmplx_twiddle_table_256_q15;

    // copy windowed frame to FFT-aligned buffer
    //memcpy(fft_data, result, sizeof(result));

    // Real forward FFT with auto-scaling. Scaling is applied as needed to prevent saturation of the output
    // y = fft( real (x) ) * 2^-shift
    // result  = * 2^shift

    //msp_benchmarkStart(MSP_BENCHMARK_BASE, 64);
    status = msp_fft_auto_q15(&fftParams, fft_data, shift);
    //cycleCount = msp_benchmarkStop(MSP_BENCHMARK_BASE);

}

void power_spectrum(){

    tmpR = 0;
    addCntr = 0;
    add = 0;

    for(i=0;i<frame_length;i++)
     {
        tmpR += (int32_t)fft_data[i] * (int32_t)fft_data[i];

        if(add == 1){
            //pow_spectrum[addCntr] = (tmpR >> 8);// divide by frame_length
            pow_spectrumIQ8[addCntr] = _IQ8(tmpR >> 8);

            addCntr++;
            add = 0;
            tmpR = 0;
        }else
            add++;
     }

}


void logMelFilterBank(){


//    for ( i=0; i<nfilt; i++) {
//
//        lmfCoefTemp = _IQ8(0.0);
//
//         for (k=0; k<maxNonZeroFbank; k++){
//            lmfCoefTemp  += _IQ8mpy(fbankIQ8[i][indexes[i][k]], pow_spectrumIQ8[indexes[i][k]]);
//         }
//
//        lmfCoefTemp = _IQ8log(lmfCoefTemp);
//        // Multiply by lmfbCoef_scale_1024 to avoid overflow
//        lmfbCoefQ31[i] = _IQ31(_IQ8toF(lmfCoefTemp) * lmfbCoef_scale_1024);
//    }


    lmfCoefTemp = 0;

      while(filtNum < nfilt){

          if(indexList[indCounter] == 0 ){

              lmfCoefTemp = _IQ8log(lmfCoefTemp);


              lmfbCoefQ31[filtNum] = _IQ31(_IQ8toF(lmfCoefTemp) * lmfbCoef_scale_1024);
              lmfCoefTemp = 0; //_IQ8(0.0);
              filtNum++;
          }else{
              lmfCoefTemp+= _IQ8mpy(fbankIQ8[filtNum][indexList[indCounter]], pow_spectrumIQ8[indexList[indCounter]]);
          }
          indCounter++;

      }

      filtNum = 0;
      indCounter = 0;


}


void apply_DCT(){


    memcpy(cosinesDSP, cosinesQ31, sizeof(cosinesQ31));
    memcpy(lmfbCoefDSP, lmfbCoefQ31, sizeof(lmfbCoefQ31));

    iq31_params.srcARows = numCepstra + 1;
    iq31_params.srcACols = nfilt;
    iq31_params.srcBRows = nfilt;
    iq31_params.srcBCols = 1;


    status = msp_matrix_mpy_iq31(&iq31_params, *cosinesDSP, *lmfbCoefDSP, *mfccDSP);
    //msp_checkStatus(status);

     for (i=0; i<numCepstra+1; i++) {
         mfccs[i] = (_IQ30toF(mfccDSP[i][0]))*fn_x_1024;
    }
    // Re-estimate the first coefficient using different scaling factor
    mfccs[0] *= f0_fn_ratio;



}


