

#include "dfs.h"
#include <stdio.h>


    //! \param dcofsel selects valid frequency options based on dco frequency range
    //!        selection (dcorsel)
    //!        Valid values are:
    //!        - \b CS_DCOFSEL_0 - Low frequency option 1MHz. High frequency option
    //!           1MHz.
    //!        - \b CS_DCOFSEL_1 - Low frequency option 2.67MHz. High frequency
    //!           option 5.33MHz.
    //!        - \b CS_DCOFSEL_2 - Low frequency option 3.33MHz. High frequency
    //!           option 6.67MHz.
    //!        - \b CS_DCOFSEL_3 - Low frequency option 4MHz. High frequency option
    //!           8MHz.
    //!        - \b CS_DCOFSEL_4 - Low frequency option 5.33MHz. High frequency
    //!           option 16MHz.
    //!        - \b CS_DCOFSEL_5 - Low frequency option 6.67MHz. High frequency
    //!           option 20MHz.
    //!        - \b CS_DCOFSEL_6 - Low frequency option 8MHz. High frequency option
    //!           24MHz.
    //!
    //! \return None



 // set MCLK to 1 MHz
void set_MCLK_freq_1(void){

    CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_0);
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);

   // CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
}
// set MCLK to 2.67 MHz
void set_MCLK_freq_2(void){

    CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_1);
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
}
// set MCLK to 3.33 MHz
void set_MCLK_freq_3(void){

    CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_2);
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
}
// set MCLK to 4 MHz
void set_MCLK_freq_4(void){

    CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_3);
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
}
// set MCLK to 5.33 MHz
void set_MCLK_freq_5(void){

    CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_4);
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
}
// set MCLK to 6.67 MHz
void set_MCLK_freq_6(void){

    CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_5);
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
}
// set MCLK to 8 MHz
void set_MCLK_freq_8(void){

    CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_6);
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
}

// set MCLK to 16 MHz
void set_MCLK_freq_16(void){

    FRCTL0 = FRCTLPW | NWAITS_1;
    CS_setDCOFreq(CS_DCORSEL_1, CS_DCOFSEL_4);
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
}




