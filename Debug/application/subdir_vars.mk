################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application/application.c \
../application/audio_collect.c \
../application/audio_playback.c \
../application/dac8311.c \
../application/dfs.c \
../application/mfcc.c 

C_DEPS += \
./application/application.d \
./application/audio_collect.d \
./application/audio_playback.d \
./application/dac8311.d \
./application/dfs.d \
./application/mfcc.d 

OBJS += \
./application/application.obj \
./application/audio_collect.obj \
./application/audio_playback.obj \
./application/dac8311.obj \
./application/dfs.obj \
./application/mfcc.obj 

OBJS__QUOTED += \
"application\application.obj" \
"application\audio_collect.obj" \
"application\audio_playback.obj" \
"application\dac8311.obj" \
"application\dfs.obj" \
"application\mfcc.obj" 

C_DEPS__QUOTED += \
"application\application.d" \
"application\audio_collect.d" \
"application\audio_playback.d" \
"application\dac8311.d" \
"application\dfs.d" \
"application\mfcc.d" 

C_SRCS__QUOTED += \
"../application/application.c" \
"../application/audio_collect.c" \
"../application/audio_playback.c" \
"../application/dac8311.c" \
"../application/dfs.c" \
"../application/mfcc.c" 


